export default {
  namespaced: true,
  state: {
    isMobile: false,
    theme: 'dark',
    layout: 'side',
    systemName: 'Blacklist',
    // env: {
    //   DEV_API: 'http://localhost:8000/api/',
    //   PROD_API: '208.87.2.124/blacklist.api'
    // },
    copyright: ` ${new Date().getFullYear()} - Grupo Maruma`,
    footerLinks: [
      {link: '', name: ''}
    ],
    multipage: true
  },
  mutations: {
    setDevice (state, isMobile) {
      state.isMobile = isMobile
    },
    setTheme (state, theme) {
      state.theme = theme
    },
    setLayout (state, layout) {
      state.layout = layout
    },
    setMultipage (state, multipage) {
      state.multipage = multipage
    }
  }
}
