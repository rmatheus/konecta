require('./bootstrap');
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import MainApp from './components/MainApp.vue';
import {routes} from './routes';
import App from './App.vue';

import 'ant-design-vue/dist/antd.css';
import Antd from 'ant-design-vue';

import axios from 'axios';
import router from './router/lazy';
import store from './store'


Vue.prototype.$axios = axios;
Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(Antd);

const app = new Vue({
    el: '#app',
    router,
    store,
    components: {
        App
    },
    template: '<App/>',
});

