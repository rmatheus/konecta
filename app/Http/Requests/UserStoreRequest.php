<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;

class UserStoreRequest extends FormRequest
{
    public function authorize()
    {
       $user = User::find($this->user['id']);
       return $user->hasPermissionTo('create_user'); 
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|required',
            'email' => 'string|unique:users',
            'document' => 'string|unique:users',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'A name is required',
            'email.unique' => 'email must be unique',
            'document.unique' => 'document must be unique',
            'email.required' => 'A email is required',
            'document.required' => 'A document is required',
        ];
    }
}
