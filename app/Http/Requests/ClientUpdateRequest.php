<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;

class ClientUpdateRequest extends FormRequest
{
    public function authorize()
    {
       $user = User::find($this->user['id']);
       return $user->hasPermissionTo('update_client'); 
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
            'email' => 'string',
            'document' => 'string',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'A name is required',
            'email.unique' => 'email must be unique',
            'document.unique' => 'document must be unique',
            'email.required' => 'A email is required',
            'document.required' => 'A document is required',
        ];
    }
}
