<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ClientStoreRequest;
use App\Http\Requests\ClientUpdateRequest;
use App\Http\Requests\QuerySearchRequest;
use Spatie\Permission\Models\Permission;
use DB;

use App\User;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {        
        $users = User::role('client')->get();

        return $users;
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store(ClientStoreRequest $request)
    {
        $userData = $request->all();
        $userData['password'] = 'root';
        $user = User::create($userData);
        $user->assignRole('client');
        return $user;
    }

    /**
     * update a created resource in storage.
     *
     */
    public function update(ClientUpdateRequest $request, $id)
    {
        $userData = $request->all();
        $usr = User::findOrFail($id);
        
        $usr->name = $userData['name']?? $usr->name;
        $usr->document = $userData['document']?? $usr->document;
        $usr->email = $userData['email']?? $usr->email;

        $usr->save();

        return $usr;
    }

    /**
     * update a created resource in storage.
     *
     */
    public function destroy(Request $request, $id)
    {
        $user = User::find($request->user['id']);
        if(!$user->hasPermissionTo('delete_client')) {
            return response()->json([],403);
        }
        $usr = User::findOrFail($id);

        $usr->delete();

        return ['msg' => 'ok'];
    }

    public function search(QuerySearchRequest $request)
    {
        $query = $request->get('query');
        $sql = "'%$query%'";

        $clientes = DB::table('users')
            ->join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
            ->join('roles', function ($join) {
                $join->on('model_has_roles.role_id', '=', 'roles.id')
                     ->where('roles.name', 'client');
            })
            ->whereRaw("lower(users.name) like lower($sql)")
            ->orWhereRaw("lower(users.email) like lower($sql)")
            ->orWhereRaw("lower(users.document) like lower($sql)")
            ->orderBy('users.id', 'desc')
            ->select('users.id', 'users.name', 'users.email', 'users.document')
            ->get()->toArray();

        return $clientes;
    }
}
